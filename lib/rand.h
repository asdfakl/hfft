#ifndef HFFT_RAND
#define HFFT_RAND

#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

ssize_t rand_read(void *p, size_t len);

#endif
