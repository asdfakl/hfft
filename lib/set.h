#ifndef HFFT_SET
#define HFFT_SET

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <arpa/inet.h>

typedef struct set {
	int len; // TODO change to uint32_t
	uint8_t *data;
} set_t;

set_t* set_make(int len);
void set_free(set_t *s);

int set_set(set_t *s, int offset);
int set_unset(set_t *s, int offset);

int set_get(set_t *s, int offset);

int set_count(set_t *s);

size_t set_len_serialized(set_t *s);

char* set_serialize(set_t *s);
set_t* set_deserialize(char *buf, size_t len);

void set_print(set_t *s); // debug

#endif
