#include "set.h"

static size_t byte_len(int len) {
	if (len <= 0) {
		return 0;
	}

	size_t s = len / 8;
	return (len % 8) ? s + 1 : s; 
}

set_t* set_make(int len) {
	assert(sizeof(uint8_t) == 1); // just in case...
	if (len <= 0) {
		return NULL;
	}

	set_t *s = (set_t*)malloc(sizeof(set_t));
	if (!s) {
		return NULL;
	}
	s->len = len;

	s->data = NULL;
	size_t data_size = byte_len(len);
	uint8_t *data = (uint8_t*)malloc(data_size);
	if (!data) {
		set_free(s);
		return NULL;
	}
	memset(data, 0, data_size);
	s->data = data;

	return s;
}

void set_free(set_t *s) {
	if (s->data) {
		free(s->data);
	}
	free(s);
}

static int set(set_t *s, int offset, int val) {
	if (offset < 0 || offset >= s->len) {
		return 1;
	}

	int byte_offset = offset / 8;
	int bit_offset = offset % 8;

	if (val) {
		int mask = 1 << bit_offset;
		s->data[byte_offset] |= mask;
	} else {
		int mask = ~(1 << bit_offset);
		s->data[byte_offset] &= mask;
	}

	return 0;
}

int set_set(set_t *s, int offset) {
	return set(s, offset, 1);
}

int set_unset(set_t *s, int offset) {
	return set(s, offset, 0);
}

int set_get(set_t *s, int offset) {
	if (offset < 0 || offset >= s->len) {
		return 0;
	}

	int byte_offset = offset / 8;
	int mask = 1 << (offset % 8);

	int val = s->data[byte_offset] & mask;

	return val;
}

int set_count(set_t *s) {
	int count = 0;
	uint64_t swab;
	size_t swab_size = sizeof(swab);
	int i = 0;
	size_t len = byte_len(s->len);

	while (i < len) {
		if (i < len - 8) {
			memcpy(&swab, &s->data[i], swab_size);
			if (!swab) {
				i += 8;
				continue;
			}
		}

		if (s->data[i]) {
			int mask = 1;
			for (int j = 0; j < 8; j++) {
				if (s->data[i] & mask) {
					count++;
				}
				mask <<= 1;
			}
		}
		i++;
	}

	return count;
}

size_t set_len_serialized(set_t *s) {
	return sizeof(uint32_t) + byte_len(s->len);
}

char* set_serialize(set_t *s) {
	char *buf = (char*)malloc(set_len_serialized(s));
	assert(buf);

	uint32_t len = htonl(s->len);
	memcpy(buf, &len, sizeof(uint32_t));

	memcpy(buf + sizeof(uint32_t), s->data, byte_len(s->len));

	return buf;
}

set_t* set_deserialize(char *buf, size_t buf_len) {
	if (buf_len < sizeof(uint32_t)) {
		return NULL;
	}

	uint32_t set_len;
	memcpy(&set_len, buf, sizeof(uint32_t));
	set_len = ntohl(set_len);

	size_t len_serialized = sizeof(uint32_t) + byte_len(set_len);
	if (len_serialized != buf_len) {
		return NULL;
	}

	set_t *s = set_make(set_len);
	assert(s);

	memcpy(s->data, buf + sizeof(uint32_t), byte_len(set_len));

	return s;
}

void set_print(set_t *s) {
	size_t len = byte_len(s->len);
	int first = 1;

	printf("[");
	for (int i = 0; i < len; i++) {
		if (s->data[i]) {
			int mask = 1;
			for (int j = 0; j < 8; j++) {
				if (s->data[i] & mask) {
					if (first) {
						printf("%d", i * 8 + j);
						first = 0;
					} else {
						printf(", %d", i * 8 + j);
					}
				}
				mask <<= 1;
			}
		}
	}
	printf("]\n");
}
