#include "queue.h"

queue_t* queue_make(int len) {
	if (len <= 1) {
		return NULL;
	}

	queue_t *q = (queue_t*)malloc(sizeof(queue_t));
	if (!q) {
		return NULL;
	}

	q->len = len;
	q->r = 0;
	q->w = 0;

	void **buf = (void**)malloc(len * sizeof(void*));
	if (!buf) {
		queue_free(q);
		return NULL;
	}
	memset(buf, 0, len * sizeof(void*));
	q->buf = buf;

	if (pthread_mutex_init(&q->mutex, NULL)) {
		queue_free(q);
		return NULL;
	}
	if (pthread_cond_init(&q->not_empty, NULL)) {
		queue_free(q);
		return NULL;
	}
	if (pthread_cond_init(&q->not_full, NULL)) {
		queue_free(q);
		return NULL;
	}

	return q;
}

void queue_free(queue_t *q) {
	if (!q) {
		return;
	}

	if (q->buf) {
		free(q->buf);
	}
	pthread_mutex_destroy(&q->mutex);
	pthread_cond_destroy(&q->not_empty);
	pthread_cond_destroy(&q->not_full);
	free(q);
}

static int incr(queue_t *q, int i) {
	return ++i % q->len;
}

static int queue_isempty(queue_t *q) {
	return q->w == q->r;
}

static int queue_isfull(queue_t *q) {
	return incr(q, q->w) == q->r;
}

void queue_push(queue_t *q, void *val) {
	assert(val);
	assert(!pthread_mutex_lock(&q->mutex));

	while (queue_isfull(q)) {
		assert(!pthread_cond_wait(&q->not_full, &q->mutex));
	}

	q->buf[q->w] = val;
	q->w = incr(q, q->w);
	assert(!pthread_mutex_unlock(&q->mutex));
	assert(!pthread_cond_signal(&q->not_empty));
}

void* queue_pop(queue_t *q) {
	assert(!pthread_mutex_lock(&q->mutex));

	while (queue_isempty(q)) {
		assert(!pthread_cond_wait(&q->not_empty, &q->mutex));
	}

	void *val = q->buf[q->r];
	q->r = incr(q, q->r);
	assert(!pthread_mutex_unlock(&q->mutex));
	assert(!pthread_cond_signal(&q->not_full));
	return val;
}

void queue_print(queue_t *q) {
	assert(!pthread_mutex_lock(&q->mutex));

	printf("%d\t%d\t%d\t%d\t%d\n", q->len, q->r, q->w, queue_isempty(q), queue_isfull(q));
	for (int i = 0; i < q->len; i++) {
		printf("%d\t%p\n", i, q->buf[i]);
	}

	assert(!pthread_mutex_unlock(&q->mutex));
}
