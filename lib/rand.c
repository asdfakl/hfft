#include "rand.h"

ssize_t rand_read(void *p, size_t len) {
	int fd = open("/dev/urandom", O_RDONLY);
	if (fd == -1) {
		return -1;
	}

	ssize_t rlen = read(fd, p, len);
	close(fd);
	return rlen;
}
