#include <unistd.h>
#include <time.h>
#include "lib.h"

#define NUM_NUMBS 100
#define RAND_DEBUG 0

static int numbers[NUM_NUMBS];

void* run_producer(void *arg) {
	queue_t *q = (queue_t*)arg;

	for (int i = 0; i < NUM_NUMBS; i++) {
		queue_push(q, (void*) &numbers[i]);
	}
	return NULL;
}

void* run_consumer(void *arg) {
	queue_t *q = (queue_t*)arg;

	for (int i = 0; i < NUM_NUMBS; i++) {
		void *val = queue_pop(q);
		assert(numbers[i] == *(int*) val);
	}
	return NULL;
}

int test_queue() {
	int arr[5] = {1, 2, 3, 4, 5};

	queue_t *q = queue_make(6);
	assert(q);

	queue_push(q, (void*)&arr[0]);
	queue_push(q, (void*)&arr[1]);
	queue_push(q, (void*)&arr[2]);
	queue_push(q, (void*)&arr[3]);
	queue_push(q, (void*)&arr[4]);

	assert(*(int*)queue_pop(q) == 1);
	assert(*(int*)queue_pop(q) == 2);
	assert(*(int*)queue_pop(q) == 3);
	assert(*(int*)queue_pop(q) == 4);
	assert(*(int*)queue_pop(q) == 5);

	queue_push(q, (void*)&arr[2]);
	assert(*(int*)queue_pop(q) == 3);

	for (int i = 0; i < NUM_NUMBS; i++) {
		numbers[i] = i + 1;
	}

	pthread_t producer, consumer;
	assert(!pthread_create(&producer, NULL, run_producer, (void*) q));
	assert(!pthread_create(&consumer, NULL, run_consumer, (void*) q));

	pthread_join(producer, NULL);
	pthread_join(consumer, NULL);

	queue_free(q);
	return 0;
}

long time_diff(struct timespec *start, struct timespec *stop) {
	return (stop->tv_sec - start->tv_sec) * 1e9 + stop->tv_nsec - start->tv_nsec; // will fail on large time diffs
}

int test_set() {
	assert(!set_make(-1));
	assert(!set_make(0));

	set_t *s = set_make(20);
	assert(s);

	assert(set_count(s) == 0);

	assert(!set_set(s, 0));
	assert(!set_set(s, 5));
	assert(!set_set(s, 10));
	assert(!set_set(s, 19));
	assert(set_set(s, -1));
	assert(set_set(s, 20));

	assert(set_count(s) == 4);

	assert(set_get(s, 0));
	assert(set_get(s, 5));
	assert(set_get(s, 10));
	assert(set_get(s, 19));
	assert(!set_get(s, -1));
	assert(!set_get(s, 3));
	assert(!set_get(s, 13));
	assert(!set_get(s, 18));
	assert(!set_get(s, 20));

	assert(!set_unset(s, 0));
	assert(!set_unset(s, 7));
	assert(!set_unset(s, 10));

	assert(set_count(s) == 2);

	assert(set_get(s, 5));
	assert(set_get(s, 19));
	assert(!set_get(s, 0));
	assert(!set_get(s, 10));

	set_free(s);
	return 0;
}

int test_set_serialization() {
	set_t *src = set_make(20);
	assert(src);

	assert(!set_set(src, 0));
	assert(!set_set(src, 3));
	assert(!set_set(src, 7));
	assert(!set_set(src, 10));
	assert(!set_set(src, 12));
	assert(!set_set(src, 15));
	assert(!set_set(src, 19));

	assert(set_count(src) == 7);

	char *buf = set_serialize(src);
	set_free(src);
	assert(buf);

	assert(!set_deserialize(buf, 0));
	assert(!set_deserialize(buf, 5));
	assert(!set_deserialize(buf, 10232131));

	set_t *dst = set_deserialize(buf, 7);
	assert(dst);
	free(buf);

	assert(dst->len = 20);

	assert(set_get(dst, 0));
	assert(set_get(dst, 3));
	assert(set_get(dst, 7));
	assert(set_get(dst, 10));
	assert(set_get(dst, 12));
	assert(set_get(dst, 15));
	assert(set_get(dst, 19));

	assert(set_count(dst) == 7);

	set_free(dst);
	return 0;
}

int test_set_perf() {
	set_t *s = set_make(2e6 + 3);

	assert(set_count(s) == 0);

	assert(!set_set(s, 0));
	assert(!set_set(s, 156965));
	assert(!set_set(s, 156966));
	assert(!set_set(s, 243145));
	assert(!set_set(s, 457600));
	assert(!set_set(s, 660324));
	assert(!set_set(s, 660323));
	assert(!set_set(s, 890991));
	assert(!set_set(s, 999999));

	assert(set_count(s) == 9);

	struct timespec start_time, stop_time;
	clock_gettime(CLOCK_MONOTONIC, &start_time);

	for (int i = 0; i < 10000; i++) {
		assert(set_count(s) == 9);
	}

	clock_gettime(CLOCK_MONOTONIC, &stop_time);

	printf("set count performance: %ld ns\n", time_diff(&start_time, &stop_time) / 10000);

	set_free(s);
	return 0;
}

int test_rand() {
	int i, j;
	char mod;
	ssize_t rlen = 0;

	int t_int = 0;
	for (i = 0; i < 5; i++) {
		rlen = rand_read((void*) &t_int, sizeof(t_int));
		assert(rlen == sizeof(t_int));
		if (RAND_DEBUG) {
			printf("int\t\t%d\t\t%lu B\n", t_int, rlen);
		}
	}

	unsigned int t_unsigned_int = 0;
	for (i = 0; i < 5; i++) {
		rlen = rand_read((void*) &t_unsigned_int, sizeof(t_unsigned_int));
		assert(rlen == sizeof(t_unsigned_int));
		if (RAND_DEBUG) {
			printf("unsigned int\t%d\t\t%lu B\n", t_unsigned_int, rlen);
		}
	}

	uint8_t t_uint8_t = 0;
	for (i = 0; i < 5; i++) {
		rlen = rand_read((void*) &t_uint8_t, sizeof(t_uint8_t));
		assert(rlen == sizeof(t_uint8_t));
		if (RAND_DEBUG) {
			printf("uint8_t\t\t%u\t\t\t%lu B\n", t_uint8_t, rlen);
		}
	}

	char t_char_arr[21];
	for (i = 0; i < 5; i++) {
		rlen = rand_read((void*) t_char_arr, sizeof(t_char_arr));
		assert(rlen == sizeof(t_char_arr));

		for (j = 0; j < sizeof(t_char_arr) - 1; j++) {
			mod = t_char_arr[j] % 26;
			t_char_arr[j] = 97 + (mod < 0 ? -mod : mod);
		}
		t_char_arr[20] = '\0';

		if (RAND_DEBUG) {
			printf("char[21]\t%s\t%lu B\n", t_char_arr, rlen);
		}
	}

	return 0;
}

int main() {
	int retval;
	if ((retval = test_queue())) {
		return retval;
	}
	if ((retval = test_set())) {
		return retval;
	}
	if ((retval = test_set_serialization())) {
		return retval;
	}
	if ((retval = test_rand())) {
		return retval;
	}
	if (getenv("TEST_PERFORMANCE")) {
		if ((retval = test_set_perf())) {
			return retval;
		}
	}

	return 0;
}
