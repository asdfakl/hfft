#ifndef HFFT_QUEUE
#define HFFT_QUEUE

#include <pthread.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

// circular buffer queue with capacity len - 1
typedef struct queue {
	int len;
	int r;
	int w;
	void **buf;
	pthread_mutex_t mutex;
	pthread_cond_t not_empty;
	pthread_cond_t not_full;
} queue_t;

queue_t* queue_make(int len);
void queue_free(queue_t *q);

void queue_push(queue_t *q, void *val);
void* queue_pop(queue_t *q);

void queue_print(queue_t *q); // debug

#endif
