#include "hfft.h"

char* serialize_hdr(hdr_t *hdr) {
	char *buf = (char*)malloc(LEN_HDR);
	if (!buf) {
		die("serialize_hdr malloc");
	}

	memset(buf, 0, LEN_HDR);

	memcpy(buf, &hdr->msg_type, sizeof(uint8_t));

	uint16_t upload_number = htons(hdr->upload_number);
	memcpy(buf + 1, &upload_number, sizeof(uint16_t));

	uint32_t chunk_number = htonl(hdr->chunk_number);
	memcpy(buf + 3, &chunk_number, sizeof(uint32_t));

	uint32_t packet_number = htonl(hdr->packet_number);
	memcpy(buf + 7, &packet_number, sizeof(uint32_t));

	return buf;
}

hdr_t* deserialize_hdr(char *buf) {
	hdr_t *hdr = (hdr_t*)malloc(sizeof(hdr_t));
	if (!hdr) {
		die("deserialize_hdr malloc");
	}

	memset(hdr, 0, sizeof(hdr_t));

	hdr->msg_type = (uint8_t)buf[0];
	buf += sizeof(uint8_t);

	uint16_t upload_number;
	memcpy(&upload_number, buf, sizeof(uint16_t));
	hdr->upload_number = ntohs(upload_number);
	buf += sizeof(uint16_t);

	uint32_t chunk_number;
	memcpy(&chunk_number, buf, sizeof(uint32_t));
	hdr->chunk_number = ntohl(chunk_number);
	buf += sizeof(uint32_t);

	uint32_t packet_number;
	memcpy(&packet_number, buf, sizeof(uint32_t));
	hdr->packet_number = ntohl(packet_number);
	buf += sizeof(uint32_t); // unnecessary

	return hdr;
}

void print_hdr(hdr_t *hdr) {
	printf("msg_type\t%u\n", hdr->msg_type);
	printf("upload_number\t%u\n", hdr->upload_number);
	printf("chunk_number\t%u\n", hdr->chunk_number);
	printf("packet_number\t%u\n", hdr->packet_number);
}
