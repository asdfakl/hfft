#include "hfft.h"

static rcv_ctx_t ctx;
static int server_socket = 0;
static queue_t *server_queue = NULL;

void do_listen(char *aip, char *aport) {
	/* parse */
	ia_t ip;
	if (!inet_aton(aip, &ip)) {
		die("parse ip");
	}

	uint16_t port = (uint16_t)atoi(aport);
	if (!port) {
		die("parse port");
	}

	/* init resources */
	struct sockaddr_in bind_addr;

	server_queue = queue_make(SERVER_QUEUE_SIZE);
	if (!server_queue) {
		die("make_queue");
	}

	ctx.upload = NULL;
	ctx.packet_set = NULL;
	ctx.fd = -1;
	ctx.chunk_number = 0;

	// create UDP socket
	if ((server_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		die("socket");
	}
	set_rcv_buf_len(server_socket);

	memset((char *) &bind_addr, 0, sizeof(bind_addr));

	bind_addr.sin_family = AF_INET;
	bind_addr.sin_port = htons(port);
	bind_addr.sin_addr = ip;

	if (bind(server_socket, (sa_t*) &bind_addr, sizeof(bind_addr)) == -1) {
		die("bind");
	}

	pthread_t listener;
	if (pthread_create(&listener, NULL, run_server_listen, (void*) &bind_addr)) {
		die("start run_server_listen");
	}

	/* begin */
	while (1) {
		raw_t *raw = (raw_t*)queue_pop(server_queue);
		handle_packet(raw);
	}

	/* shutdown */
	if (pthread_cancel(listener)) {
		die("listener cancel");
	}
	pthread_join(listener, NULL);

	if (ctx.upload) {
		free(ctx.upload);
	}
	if (ctx.fd >= 0) {
		close(ctx.fd);
	}
	if (ctx.packet_set) {
		set_free(ctx.packet_set);
	}

	// TODO drain queue
	queue_free(server_queue);
	close(server_socket);
}

void* run_server_listen(void *arg) {
	sai_t listen_addr = *(sai_t*)arg;
	sai_t remote_addr;
	int recv_len;
	unsigned int slen = sizeof(remote_addr);
	char buf[BUF_LEN];

	printf("listening on %s:%u\n", inet_ntoa(listen_addr.sin_addr), ntohs(listen_addr.sin_port));

	while (1) {
		if ((recv_len = recvfrom(server_socket, buf, BUF_LEN, 0, (sa_t*) &remote_addr, &slen)) == -1) {
			die("receive");
		}
		if (recv_len < LEN_HDR) {
			continue;
		}

		raw_t *raw = (raw_t*)malloc(sizeof(raw_t));
		if (!raw) {
			die("malloc");
		}
		raw->len = recv_len;
		raw->ip = remote_addr.sin_addr;
		raw->port = ntohs(remote_addr.sin_port);
		raw->buf = (char*)malloc(recv_len);
		if (!raw->buf) {
			die("malloc");
		}
		memcpy(raw->buf, buf, recv_len);

		queue_push(server_queue, (void*)raw);
	}
	return NULL;
}

void handle_packet(raw_t *raw) {
	if (raw->len < LEN_HDR) {
		free(raw->buf);
		free(raw);
		return;
	}

	hdr_t *hdr = deserialize_hdr(raw->buf);

	switch (hdr->msg_type) {
		case MSG_REQUEST_UPLOAD:
			handle_request_upload(raw->buf + LEN_HDR, raw->len - LEN_HDR, raw->ip, raw->port);
			break;
		case MSG_END_UPLOAD:
			handle_end_upload(hdr, raw->ip, raw->port);
			break;
		case MSG_DATA:
			handle_data(hdr, raw->buf + LEN_HDR, raw->len - LEN_HDR, raw->ip, raw->port);
			break;
		case MSG_SET:
			handle_set(hdr, raw->ip, raw->port);
			break;
		case MSG_PING:
			printf("ping %u (%d bytes) from %s:%u\n", hdr->packet_number, raw->len, inet_ntoa(raw->ip), raw->port);
			handle_ping(hdr, raw->ip, raw->port);
			break;
		default:
			printf("unsupported msg type %u\n", hdr->msg_type);
			break;
	}

	free(hdr);
	free(raw->buf);
	free(raw);
}

void handle_request_upload(char *buf, int len, ia_t ip, uint16_t port) {
	if (len != LEN_UPLOAD_SERIAL) {
		return;
	}
	upload_t *upload = deserialize_upload(buf);

	if (upload->fname[LEN_FNAME - 1]) {
		printf("upload filename overflow\n");
		reject_upload(upload, ip, port);
		free(upload);
		return;
	}

	if (ctx.upload) {
		if (ctx.upload->number == upload->number) {
			ack_upload(upload, ip, port);
		} else {
			reject_upload(upload, ip, port);
		}
		free(upload);
		return;
	}

	ctx.fd = open(upload->fname, O_CREAT|O_WRONLY|O_TRUNC);
	if (ctx.fd < 0) {
		printf("could not open %s\n", upload->fname);
		reject_upload(upload, ip, port);
		free(upload);
		return;
	}
	// TODO add permission bits to upload struct
	if (chmod(upload->fname, 0644)) {
		printf("could not set permissions for %s\n", upload->fname);
		reject_upload(upload, ip, port);
		close(ctx.fd);
		ctx.fd = -1;
		free(upload);
		return;
	}

	printf("accepted upload number %u from %s:%u (%lu bytes)\n", upload->number, inet_ntoa(ip), port, len_upload(upload));
	ack_upload(upload, ip, port);
	ctx.upload = upload;
	ctx.chunk_number = 0;
	ctx.packet_set = set_make(packet_count(ctx.chunk_number, len_upload(upload)));
	if (!ctx.packet_set) {
		die("set make");
	}
}

void handle_end_upload(hdr_t *hdr, ia_t ip, uint16_t port) {
	if (ctx.upload && ctx.upload->number == hdr->upload_number) {
		printf("upload number %u terminated\n", ctx.upload->number);
		if (ctx.fd >= 0) {
			close(ctx.fd);
			ctx.fd = -1;
		}
		if (ctx.packet_set) {
			set_free(ctx.packet_set);
			ctx.packet_set = NULL;
		}
		free(ctx.upload);
		ctx.upload = NULL;
	}

	// ack anyway regardless if msg had any effect or not
	hdr_t reply;
	reply.msg_type = MSG_ACK_END_UPLOAD;
	reply.upload_number = hdr->upload_number;

	sai_t send_addr = from_ip_port(ip, port);

	if (send_msg(server_socket, &reply, NULL, 0, (sa_t*) &send_addr)) {
		die("handle_end_upload");
	}
}

void handle_data(hdr_t *hdr, char *buf, int len, ia_t ip, uint16_t port) {
	if (!ctx.upload || ctx.upload->number != hdr->upload_number || ctx.fd < 0 || !ctx.packet_set) {
		return;
	}
	if (hdr->chunk_number == ctx.chunk_number + 1 && hdr->chunk_number < chunk_count(len_upload(ctx.upload))) {
		// next chunk, new packet set
		set_free(ctx.packet_set);
		ctx.chunk_number++;
		ctx.packet_set = set_make(packet_count(ctx.chunk_number, len_upload(ctx.upload)));
		if (!ctx.packet_set) {
			die("set make");
		}
	}

	if (hdr->chunk_number != ctx.chunk_number) {
		return;
	}
	if (set_get(ctx.packet_set, hdr->packet_number)) {
		return;
	}

	uint32_t plen = packet_len(ctx.chunk_number, hdr->packet_number, len_upload(ctx.upload));
	if (len <= 0 || len != plen) {
		fprintf(stderr, "packet length mismatch, chunk %u, packet %u, length %d, expected %u\n", ctx.chunk_number, hdr->packet_number, len, plen);
		return;
	}

	off_t offset = packet_offset(hdr->chunk_number, hdr->packet_number);

	off_t off = lseek(ctx.fd, offset, SEEK_SET);
	if (off != offset) {
		die("seek");
	}

	ssize_t n = write(ctx.fd, buf, len);
	if (n != len) {
		die("write");
	}

	if (set_set(ctx.packet_set, hdr->packet_number)) {
		die("set set");
	}
}

void handle_set(hdr_t *hdr, ia_t ip, uint16_t port) {
	if (!ctx.upload || ctx.upload->number != hdr->upload_number || !ctx.packet_set) {
		return;
	}

	size_t len = set_len_serialized(ctx.packet_set);
	char *buf = set_serialize(ctx.packet_set);

	hdr_t msg;
	msg.msg_type = MSG_SET;
	msg.upload_number = ctx.upload->number;
	msg.chunk_number = ctx.chunk_number;

	sai_t send_addr = from_ip_port(ip, port);

	if (send_msg(server_socket, &msg, buf, len, (sa_t*) &send_addr)) {
		die("handle_set");
	}

	free(buf);
}

void handle_ping(hdr_t *hdr, ia_t ip, uint16_t port) {
	sai_t send_addr = from_ip_port(ip, port);

	if (send_msg(server_socket, hdr, NULL, 0, (sa_t*) &send_addr)) {
		die("handle_ping");
	}
}

void ack_upload(upload_t *upload, ia_t ip, uint16_t port) {
	hdr_t msg;
	msg.msg_type = MSG_ACK_UPLOAD;
	msg.upload_number = upload->number;

	sai_t send_addr = from_ip_port(ip, port);

	if (send_msg(server_socket, &msg, NULL, 0, (sa_t*) &send_addr)) {
		die("ack_upload");
	}
}

void reject_upload(upload_t *upload, ia_t ip, uint16_t port) {
	hdr_t msg;
	msg.msg_type = MSG_REJECT_UPLOAD;
	msg.upload_number = upload->number;

	sai_t send_addr = from_ip_port(ip, port);

	if (send_msg(server_socket, &msg, NULL, 0, (sa_t*) &send_addr)) {
		die("reject_upload");
	}
}
