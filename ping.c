#include "hfft.h"

struct ping_ctx {
	ia_t ip;
	uint16_t port;
	uint32_t packet_number;
};

static int open_ping_socket() {
	sai_t bind_addr;
	struct timeval timeout;

	timeout.tv_sec = 5;
	timeout.tv_usec = 0;

	memset(&bind_addr, 0, sizeof(sai_t));

	bind_addr.sin_family = AF_INET;
	bind_addr.sin_port = htons(CLIENT_PORT);
	bind_addr.sin_addr.s_addr = INADDR_ANY;

	return bind_socket(bind_addr, &timeout);
}

void *ping_callback(raw_t *raw, void *ctx) {
	struct timespec pong_time;
	clock_gettime(CLOCK_MONOTONIC, &pong_time);

	struct ping_ctx *pctx = (struct ping_ctx*) ctx;

	if (raw->ip.s_addr != pctx->ip.s_addr || raw->port != pctx->port) {
		printf("ignore packet from %s:%u\n", inet_ntoa(raw->ip), raw->port);
		return NULL;
	}
	if (raw->len < LEN_HDR) {
		printf("ignore packet of %d bytes\n", raw->len);
		return NULL;
	}

	int ok;
	hdr_t *reply = deserialize_hdr(raw->buf);

	if (reply->packet_number == pctx->packet_number) {
		printf("ping from %s:%u ", inet_ntoa(raw->ip), raw->port);
		ok = 1;
	} else {
		printf("ignore header with packet number %u\n", reply->packet_number);
		ok = 0;
	}
	free(reply);

	if (!ok) {
		return NULL;
	}

	struct timespec *retval = (struct timespec*)malloc(sizeof(struct timespec));
	if (!retval) {
		die("malloc");
	}
	memcpy(retval, &pong_time, sizeof(struct timespec));

	return retval;
}

void do_ping(char *aip, char *aport) {
	/* parse */
	ia_t ip;
	if (!inet_aton(aip, &ip)) {
		die("parse ip");
	}

	uint16_t port = (uint16_t)atoi(aport);
	if (!port) {
		die("parse port");
	}

	/* init resources */
	int received = 0;
	struct timespec ping_time, *pong_time;

	sai_t ping_addr;
	struct ping_ctx ctx;
	ctx.ip = ip;
	ctx.port = port;

	memset(&ping_addr, 0, sizeof(sai_t));

	ping_addr.sin_family = AF_INET;
	ping_addr.sin_port = htons(port);
	ping_addr.sin_addr = ip;

	int ping_socket = open_ping_socket();

	hdr_t ping_hdr;

	ping_hdr.msg_type = MSG_PING;
	ping_hdr.packet_number = 0;
	size_t pn_size = sizeof(ping_hdr.packet_number);
	if (rand_read((void*) &ping_hdr.packet_number, pn_size) != pn_size) {
		die("rand read");
	}

	/* ping pong */
	printf("PING %s:%u\n", inet_ntoa(ping_addr.sin_addr), ntohs(ping_addr.sin_port));
	for (int i = 0; i < PING_COUNT; i++) {
		clock_gettime(CLOCK_MONOTONIC, &ping_time);

		ctx.packet_number = ping_hdr.packet_number;
		pong_time = (struct timespec*)send_receive(ping_socket, &ping_addr, &ping_hdr, NULL, 0, 1, &ctx, ping_callback);

		if (!pong_time) {
			printf("timeout\n");
			continue;
		}

		received++;
		printf("time=%.3f ms\n", (double) time_diff(&ping_time, pong_time) / 1e6);
		free(pong_time);

		sleep(1);
		if (rand_read((void*) &ping_hdr.packet_number, pn_size) != pn_size) {
			die("rand read");
		}
	}

	float packet_loss = 100.0f * (float) (PING_COUNT - received) / PING_COUNT;
	printf("%d packets transmitted, %d received, %.f%% packet loss\n", PING_COUNT, received, packet_loss);

	/* shutdown */
	close(ping_socket);
}
