#include "hfft.h"

void die(char *s) {
	if (errno) {
		perror(s);
	} else {
		fprintf(stderr, "fatal: %s\n", s);
	}
	exit(1);
}

sai_t from_ip_port(ia_t ip, uint16_t port) {
	sai_t addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr = ip;

	return addr;
}

int send_msg(int s, hdr_t *hdr, char *buf, ssize_t len, sa_t *addr) {
	char *msg = (char*)malloc(LEN_HDR + len);
	if (!msg) {
		die("send_msg malloc");
	}
	char *ser = serialize_hdr(hdr);

	memcpy(msg, ser, LEN_HDR);
	if (len > 0) {
		memcpy(msg + LEN_HDR, buf, len);
	}

	int ok = sendto(s, msg, LEN_HDR + len, 0, addr, sizeof(struct sockaddr_in));

	free(ser);
	free(msg);

	return ok == -1 ? 1 : 0;
}

int bind_socket(sai_t bind_addr, struct timeval *timeout) {
	int s;

	if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		die("socket");
	}
	if (timeout) {
		if (setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (void*) timeout, sizeof(struct timeval))) {
			die("setsockopt SO_RCVTIMEO");
		}
	}

	if (bind(s, (sa_t*) &bind_addr, sizeof(bind_addr)) == -1) {
		die("bind");
	}

	return s;
}

sai_t parse_addr(char *aip, char *aport) {
	ia_t ip;
	if (!inet_aton(aip, &ip)) {
		die("parse ip");
	}

	uint16_t port = (uint16_t)atoi(aport);
	if (!port) {
		die("parse port");
	}

	sai_t addr;
	memset(&addr, 0, sizeof(sai_t));

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr = ip;

	return addr;
}

long time_diff(struct timespec *start, struct timespec *stop) {
	return (stop->tv_sec - start->tv_sec) * 1e9 + stop->tv_nsec - start->tv_nsec; // will fail on large time diffs
}

off_t packet_offset(uint32_t chunk_number, uint32_t packet_number) {
	return (off_t) chunk_number * CHUNK_SIZE * BLOCK_SIZE + (off_t) packet_number * BLOCK_SIZE;
}

uint32_t chunk_count(uint64_t len) {
	if (!len) {
		return 0;
	}

	uint32_t count = len / (CHUNK_SIZE * BLOCK_SIZE);
	len -= count * CHUNK_SIZE * BLOCK_SIZE;
	return len ? count + 1 : count;
}

uint32_t packet_count(uint32_t chunk_number, uint64_t len) {
	uint64_t chunk_offset = (uint64_t) chunk_number * CHUNK_SIZE * BLOCK_SIZE;
	if (chunk_offset >= len) {
		return 0;
	}

	len -= chunk_offset;
	uint32_t count = len / BLOCK_SIZE;
	len -= count * BLOCK_SIZE;
	count = len ? count + 1 : count;
	return count > CHUNK_SIZE ? CHUNK_SIZE : count;
}

uint32_t packet_len(uint32_t chunk_number, uint32_t packet_number, uint64_t len) {
	uint64_t offset = (uint64_t) chunk_number * CHUNK_SIZE * BLOCK_SIZE + (uint64_t) packet_number * BLOCK_SIZE;
	if (offset >= len) {
		return 0;
	}

	uint64_t count = len - offset;
	return count > BLOCK_SIZE ? BLOCK_SIZE : (uint32_t) count;
}

static int get_buf_len(int socket, int name) {
	int optval;
	socklen_t optlen = sizeof(optval);

	if (getsockopt(socket, SOL_SOCKET, name, (void*) &optval, &optlen)) {
		return -1;
	}

	if (optlen != sizeof(optval)) {
		fprintf(stderr, "getsockopt optlen mismatch: expected %ld, actual %u\n", sizeof(optval), optlen);
	}

	return optval;
}

int get_rcv_buf_len(int socket) {
	return get_buf_len(socket, SO_RCVBUF);
}

int get_snd_buf_len(int socket) {
	return get_buf_len(socket, SO_SNDBUF);
}

void set_rcv_buf_len(int socket) {
	int current = get_rcv_buf_len(socket);
	if (current == -1) {
		perror("getsockopt SO_RCVBUF failed");
		return;
	}
	if (current >= RCV_BUF_LEN) {
		return;
	}

	int optval = RCV_BUF_LEN;

	if (setsockopt(socket, SOL_SOCKET, SO_RCVBUF, (void*) &optval, sizeof(optval))) {
		perror("setsockopt SO_RCVBUF failed");
		return;
	}
}

void set_snd_buf_len(int socket) {
	int current = get_snd_buf_len(socket);
	if (current == -1) {
		perror("getsockopt SO_SNDBUF failed");
		return;
	}
	if (current >= SND_BUF_LEN) {
		return;
	}

	int optval = SND_BUF_LEN;

	if (setsockopt(socket, SOL_SOCKET, SO_SNDBUF, (void*) &optval, sizeof(optval))) {
		perror("setsockopt SO_SNDBUF failed");
		return;
	}
}

void* send_receive(int sck, sai_t *send_addr, hdr_t *hdr, char *buf, size_t len, int max_timeouts, void *ctx, void* (*cb)(raw_t *raw, void *ctx)) {
	if (max_timeouts == 0) {
		max_timeouts = -1;
	}

	sai_t remote_addr;
	int recv_len;
	unsigned int slen = sizeof(sai_t);
	memset(&remote_addr, 0, slen);
	char rcvbuf[BUF_LEN];

	void *retval = NULL;

	raw_t raw;

	while (!retval && max_timeouts) {
		if (send_msg(sck, hdr, buf, len, (sa_t*) send_addr)) {
			die("send");
		}

		if ((recv_len = recvfrom(sck, rcvbuf, BUF_LEN, 0, (sa_t*) &remote_addr, &slen)) == -1) {
			if (errno != EAGAIN) {
				die("receive");
			}
			if (max_timeouts > 0) {
				max_timeouts--;
			}
			continue;
		}

		raw.len = recv_len;
		raw.ip = remote_addr.sin_addr;
		raw.port = ntohs(remote_addr.sin_port);
		raw.buf = rcvbuf;

		retval = (*cb)(&raw, ctx);
	}

	return retval;
}
