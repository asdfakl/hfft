/*
 * https://www.binarytides.com/programming-udp-sockets-c-linux/
 */

#include "hfft.h"

void usage() {
	char *msg = "Usage: hfft [ [listen|ping] [IPv4] [PORT] | send [IPv4] [PORT] [FILE] ]\n";
	write(2, msg, strlen(msg));
}

char *e_sighup = "SIGHUP not ignored\n";

int main(int argc, char **argv) {
	if (argc < 2) {
		usage();
		return 1;
	}

	if (signal(SIGHUP, SIG_IGN) == SIG_ERR) {
		write(2, e_sighup, strlen(e_sighup));
	}

	if (!strcmp(argv[1], "listen")) {
		if (argc < 4) {
			usage();
			return 1;
		}
		do_listen(argv[2], argv[3]);
	} else if (!strcmp(argv[1], "send")) {
		if (argc < 5) {
			usage();
			return 1;
		}
		do_send(argv[2], argv[3], argv[4]);
	} else if (!strcmp(argv[1], "ping")) {
		if (argc < 4) {
			usage();
			return 1;
		}
		do_ping(argv[2], argv[3]);
	} else {
		usage();
		return 1;
	}

	printf("exiting\n");
	return 0;
}
