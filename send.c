#include "hfft.h"

static char* callback_success = "OK";
static char* callback_failure = "FAIL";

static int open_client_socket() {
	sai_t bind_addr;
	struct timeval timeout;

	timeout.tv_sec = 0;
	timeout.tv_usec = 500000;

	memset(&bind_addr, 0, sizeof(sai_t));

	bind_addr.sin_family = AF_INET;
	bind_addr.sin_port = htons(CLIENT_PORT);
	bind_addr.sin_addr.s_addr = INADDR_ANY;

	return bind_socket(bind_addr, &timeout);
}

void do_send(char *aip, char *aport, char *fname) {
	/* parse */
	sai_t send_addr = parse_addr(aip, aport);

	/* init resources */
	upload_t *upload = make_upload(fname);
	if (!upload) {
		die("make upload");
	}

	int client_socket = open_client_socket();
	set_snd_buf_len(client_socket);

	send_ctx_t ctx;
	ctx.socket = client_socket;
	ctx.addr = &send_addr;
	ctx.upload = upload;
	ctx.progress = NULL;
	ctx.progress_update = 0;
	ctx.window_size = INITIAL_WINDOW_SIZE;
	ctx.chunk_number = 0;
	ctx.packet_number = 0;
	if (pthread_mutex_init(&ctx.mutex, NULL)) {
		die("mutex init");
	}

	/* begin */
	printf("sending %s to %s:%d\n\n", fname, inet_ntoa(send_addr.sin_addr), ntohs(send_addr.sin_port));

	printf("*** upload ***\n");
	print_upload(upload);
	printf("\nchunk count: %u\n", chunk_count(len_upload(upload)));
	printf("*** /upload ***\n\n");

	request_upload(&ctx);

	send_file(&ctx);

	end_upload(&ctx);

	/* shutdown */
	free(upload);
	pthread_mutex_destroy(&ctx.mutex);
	if (ctx.progress) {
		set_free(ctx.progress);
	}
	close(client_socket);
}

void request_upload(send_ctx_t *ctx) {
	char *buf = serialize_upload(ctx->upload);

	hdr_t hdr;
	hdr.msg_type = MSG_REQUEST_UPLOAD;
	hdr.upload_number = ctx->upload->number;

	char *ok = (char*) send_receive(ctx->socket, ctx->addr, &hdr, buf, LEN_UPLOAD_SERIAL, 20, (void*) ctx, request_upload_callback);
	if (!ok) {
		die("request upload timeout");
	}
	printf("request upload: %s\n", ok);
	if (ok != callback_success) {
		die("upload rejected");
	}

	free(buf);
}

void* request_upload_callback(raw_t *raw, void *ctx) {
	send_ctx_t *sctx = (send_ctx_t*) ctx;

	ia_t ip = sctx->addr->sin_addr;
	uint16_t port = ntohs(sctx->addr->sin_port);

	// raw->port in host byte order
	if (raw->ip.s_addr != ip.s_addr || raw->port != port) {
		fprintf(stderr, "ignoring packet from %s:%u\n", inet_ntoa(raw->ip), raw->port);
		return NULL;
	}
	if (raw->len != LEN_HDR) {
		fprintf(stderr, "ignoring packet of %d bytes\n", raw->len);
		return NULL;
	}

	hdr_t *reply = deserialize_hdr(raw->buf);

	int accepted = 0;

	if (reply->msg_type == MSG_ACK_UPLOAD && reply->upload_number == sctx->upload->number) {
		accepted = 1;
	} else if (reply->msg_type == MSG_REJECT_UPLOAD && reply->upload_number == sctx->upload->number) {
		accepted = -1;
	}

	free(reply);

	if (accepted == 1) {
		return (void*) callback_success;
	} else if (accepted == -1) {
		return (void*) callback_failure;
	} else {
		return NULL;
	}
}

void end_upload(send_ctx_t *ctx) {
	hdr_t hdr;
	hdr.msg_type = MSG_END_UPLOAD;
	hdr.upload_number = ctx->upload->number;

	char *ok = (char*) send_receive(ctx->socket, ctx->addr, &hdr, NULL, 0, 10, (void*) ctx, end_upload_callback);
	if (!ok) {
		die("end upload timeout");
	}
	printf("end upload: %s\n", ok);
	if (ok != callback_success) {
		die("end upload failed");
	}
}

void* end_upload_callback(raw_t *raw, void *ctx) {
	send_ctx_t *sctx = (send_ctx_t*) ctx;

	ia_t ip = sctx->addr->sin_addr;
	uint16_t port = ntohs(sctx->addr->sin_port);

	if (raw->ip.s_addr != ip.s_addr || raw->port != port) {
		fprintf(stderr, "ignoring packet from %s:%u\n", inet_ntoa(raw->ip), raw->port);
		return NULL;
	}
	if (raw->len != LEN_HDR) {
		fprintf(stderr, "ignoring packet of %d bytes\n", raw->len);
		return NULL;
	}

	hdr_t *reply = deserialize_hdr(raw->buf);

	int terminated = 0;

	if (reply->msg_type == MSG_ACK_END_UPLOAD && reply->upload_number == sctx->upload->number) {
		terminated = 1;
	}

	free(reply);

	return terminated ? (void*) callback_success : NULL;
}

void send_file(send_ctx_t *ctx) {
	uint32_t num_chunks = chunk_count(len_upload(ctx->upload));

	int fd = open(ctx->upload->fname, O_RDONLY);
	if (fd < 0) {
		die("open");
	}

	// start listener
	pthread_t listener;
	if (pthread_create(&listener, NULL, run_client_listen, (void*) ctx)) {
		die("start run_client_listen");
	}

	for (uint32_t chunk_number = 0; chunk_number < num_chunks; chunk_number++) {
		// lock
		if (pthread_mutex_lock(&ctx->mutex)) {
			die("mutex lock");
		}

		ctx->chunk_number = chunk_number;
		ctx->packet_number = 0;
		if (ctx->progress) {
			set_free(ctx->progress);
			ctx->progress = NULL;
		}
		ctx->progress_update = 0;

		if (pthread_mutex_unlock(&ctx->mutex)) {
			die("mutex unlock");
		}
		// unlock

		send_chunk(ctx, fd);
		printf("chunk %u / %u complete\n", chunk_number + 1, num_chunks);
	}

	close(fd);

	// stop listener
	if (pthread_cancel(listener)) {
		die("cancel run_client_listen");
	}
	pthread_join(listener, NULL);
}

void send_chunk(send_ctx_t *ctx, int fd) {
	uint32_t pcount = packet_count(ctx->chunk_number, len_upload(ctx->upload));

	uint16_t rand;
	int receive_count = 0;
	int new_count;
	int sent_cumulative = 0;
	int sent = 0;

	hdr_t hdr;
	hdr.msg_type = MSG_SET;
	hdr.upload_number = ctx->upload->number;
	hdr.chunk_number = ctx->chunk_number;

	int ack_percentages_count = 0;
	float ack_percentages[CLIENT_NUM_ACK_PERCENTAGES];
	memset(ack_percentages, 0, sizeof(ack_percentages));

	while (1) {
		/* send window */
		sent = send_window(ctx, fd, pcount);
		sent_cumulative += sent;

		/* ask for progress */
		rand_read(&rand, sizeof(rand));
		rand %= 1000;
		if ((sent > 0 && rand < 333) || (sent <= 0 && rand < 5)) {
			if (send_msg(ctx->socket, &hdr, NULL, 0, (sa_t*) ctx->addr)) {
				die("send");
			}
		}

		/* check for progress updates */
		new_count = -1;

		// lock
		if (pthread_mutex_lock(&ctx->mutex)) {
			die("mutex lock");
		}

		if (ctx->progress && ctx->progress_update) {
			new_count = set_count(ctx->progress);
			ctx->progress_update = 0;
		}

		if (pthread_mutex_unlock(&ctx->mutex)) {
			die("mutex unlock");
		}
		// unlock

		/* congestion control */
		if (new_count >= 0) {
			if (sent_cumulative > 0) {
				float ackp = 100 * (float) (new_count - receive_count) / (float) sent_cumulative;

				if (ack_percentages_count >= CLIENT_NUM_ACK_PERCENTAGES) {
					for (int i = 0; i < CLIENT_NUM_ACK_PERCENTAGES - 1; i++) {
						ack_percentages[i] = ack_percentages[i+1];
					}
					ack_percentages_count = CLIENT_NUM_ACK_PERCENTAGES - 1;
				}
				ack_percentages[ack_percentages_count] = ackp;
				ack_percentages_count++;

				float avgp = 0;
				for (int i = 0; i < ack_percentages_count; i++) {
					avgp += ack_percentages[i];
				}
				avgp /= (float) ack_percentages_count;

				printf("window_size %u, progress %d / %u, ack percentage %.2f\n", ctx->window_size, new_count, pcount, avgp);

				if (avgp > 99.99) {
					ctx->window_size <<= 1;
				} else if (avgp > 98) {
					ctx->window_size++;
				} else if (avgp < 95 && ctx->window_size > 1) {
					uint32_t decrement = ctx->window_size / 10;
					if (!decrement) {
						decrement = 1;
					}
					ctx->window_size -= decrement;
				}
				if (ctx->window_size > MAX_WINDOW_SIZE) {
					ctx->window_size = MAX_WINDOW_SIZE;
				}
			}

			sent_cumulative = 0;
			receive_count = new_count;
			if (receive_count >= pcount) {
				break;
			}
		}

		/* sleep */
		if (CLIENT_SLEEP_US_WINDOW) {
			usleep(CLIENT_SLEEP_US_WINDOW);
		}
	}
}

void* run_client_listen(void *arg) {
	send_ctx_t *ctx = (send_ctx_t*) arg; 

	sai_t remote_addr;
	int recv_len;
	unsigned int slen = sizeof(remote_addr);
	char buf[BUF_LEN];

	raw_t raw;

	while (1) {
		if ((recv_len = recvfrom(ctx->socket, buf, BUF_LEN, 0, (sa_t*) &remote_addr, &slen)) == -1) {
			if (errno != EAGAIN) {
				die("receive");
			}
			continue;
		}

		raw.len = recv_len;
		raw.ip = remote_addr.sin_addr;
		raw.port = ntohs(remote_addr.sin_port);
		raw.buf = buf;

		client_handle_packet(ctx, &raw);
	}
	return NULL;
}

void client_handle_packet(send_ctx_t *ctx, raw_t *raw) {
	ia_t ip = ctx->addr->sin_addr;
	uint16_t port = ntohs(ctx->addr->sin_port);

	if (raw->ip.s_addr != ip.s_addr || raw->port != port) {
		fprintf(stderr, "ignoring packet from %s:%u\n", inet_ntoa(raw->ip), raw->port);
		return;
	}
	if (raw->len < LEN_HDR) {
		fprintf(stderr, "ignoring packet of %d bytes\n", raw->len);
		return;
	}

	hdr_t *reply = deserialize_hdr(raw->buf);

	if (reply->msg_type == MSG_SET && reply->upload_number == ctx->upload->number) {
		set_t *set = set_deserialize(raw->buf + LEN_HDR, raw->len - LEN_HDR);
		if (!set) {
			die("could not deserialize reply set");
		}

		// lock
		if (pthread_mutex_lock(&ctx->mutex)) {
			die("mutex lock");
		}

		if (reply->chunk_number == ctx->chunk_number) {
			if (ctx->progress) {
				set_free(ctx->progress);
			}
			ctx->progress = set;
			ctx->progress_update = 1;
		} else {
			set_free(set);
		}

		if (pthread_mutex_unlock(&ctx->mutex)) {
			die("mutex unlock");
		}
		// unlock
	}

	free(reply);
}

int send_window(send_ctx_t *ctx, int fd, uint32_t pcount) {
	hdr_t hdr;
	hdr.msg_type = MSG_DATA;
	hdr.upload_number = ctx->upload->number;
	hdr.chunk_number = ctx->chunk_number;

	char buf[BLOCK_SIZE];
	ssize_t n = 0;
	off_t offset, off;
	uint32_t plen;
	int sent = 0;
	int handled = 0; // num packets either sent or skipped
	int acked;

	// TODO align reads to block device block size
	while (sent < ctx->window_size && handled < pcount) {
		// lock
		if (pthread_mutex_lock(&ctx->mutex)) {
			die("mutex lock");
		}
		acked = 0;
		if (ctx->progress) {
			acked = set_get(ctx->progress, ctx->packet_number);
		}
		if (pthread_mutex_unlock(&ctx->mutex)) {
			die("mutex unlock");
		}
		// unlock

		if (acked) {
			handled++;
			ctx->packet_number++;
			if (ctx->packet_number >= pcount) {
				ctx->packet_number = 0;
			}
			continue;
		}

		plen = packet_len(ctx->chunk_number, ctx->packet_number, len_upload(ctx->upload));
		offset = packet_offset(ctx->chunk_number, ctx->packet_number);

		off = lseek(fd, offset, SEEK_SET);
		if (off != offset) {
			fprintf(stderr, "expecting offset %ld, got %ld\n", offset, off);
			die("lseek");
		}

		n = read(fd, buf, plen);
		if (n != plen) {
			fprintf(stderr, "expecting read %u bytes, got %ld\n", plen, n);
			die("read");
		}

		hdr.packet_number = ctx->packet_number;
		if (send_msg(ctx->socket, &hdr, buf, n, (sa_t*) ctx->addr)) {
			die("send_msg");
		}
		if (CLIENT_SLEEP_US_PACKET) {
			usleep(CLIENT_SLEEP_US_PACKET);
		}

		sent++;
		handled++;
		ctx->packet_number++;
		if (ctx->packet_number >= pcount) {
			ctx->packet_number = 0;
		}
	}

	return sent;
}
