#include "hfft.h"

upload_t* make_upload(char *fname ) {
	struct stat info;

	if (stat(fname, &info)) {
		die("stat");
	}

	if (!S_ISREG(info.st_mode)) {
		die("irregular file or directory");
	}

	upload_t *upload = (upload_t*)malloc(sizeof(upload_t));
	if (!upload) {
		die("make_upload malloc");
	}

	upload->number = 0;
	if (rand_read((void*) &upload->number, sizeof(uint16_t)) != sizeof(uint16_t)) {
		die("rand read");
	}

	upload->len_high = (0xffffffff00000000 & info.st_size) >> 32;
	upload->len_low = 0x00000000ffffffff & info.st_size;

	memset(upload->fname, 0, LEN_FNAME);
	strncpy(upload->fname, fname, LEN_FNAME);
	if (upload->fname[LEN_FNAME - 1]) {
		die("filename too long");
	}

	return upload;
}

void print_upload(upload_t *upload) {
	printf("number\t%u\n", upload->number);
	printf("len\t%lu\n", len_upload(upload));
	printf("fname\t%s\n", upload->fname);
}

char* serialize_upload(upload_t *upload) {
	char *buf = (char*)malloc(LEN_UPLOAD_SERIAL);
	if (!buf) {
		die("serialize_upload malloc"); 
	}

	memset(buf, 0, LEN_UPLOAD_SERIAL);

	uint16_t number = htons(upload->number);
	memcpy(buf, &number, sizeof(uint16_t));

	uint32_t len_high = htonl(upload->len_high);
	memcpy(buf + 2, &len_high, sizeof(uint32_t));

	uint32_t len_low = htonl(upload->len_low);
	memcpy(buf + 6, &len_low, sizeof(uint32_t));

	memcpy(buf + 10, upload->fname, LEN_FNAME);

	return buf;
}

upload_t* deserialize_upload(char *buf) {
	upload_t *upload = (upload_t*)malloc(sizeof(upload_t));
	if (!upload) {
		die("deserialize_upload malloc");
	}

	memset(upload, 0, sizeof(upload_t));

	uint16_t number;
	memcpy(&number, buf, sizeof(uint16_t));
	upload->number = ntohs(number);

	uint32_t len_high;
	memcpy(&len_high, buf + 2, sizeof(uint32_t));
	upload->len_high = ntohl(len_high);

	uint32_t len_low;
	memcpy(&len_low, buf + 6, sizeof(uint32_t));
	upload->len_low = ntohl(len_low);

	memcpy(upload->fname, buf + 10, LEN_FNAME);

	return upload;
}

uint64_t len_upload(upload_t *upload) {
	uint64_t high = ((uint64_t) upload->len_high) << 32;
	uint64_t low = (uint64_t) upload->len_low;

	return (0xffffffff00000000 & high) | (0x00000000ffffffff & low);
}
