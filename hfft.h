#ifndef HFFT
#define HFFT

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include "lib/lib.h"

#define CHUNK_SIZE          4096
#define BLOCK_SIZE          1024
#define LEN_FNAME           512
#define LEN_UPLOAD_SERIAL   522
#define LEN_HDR             11
#define BUF_LEN             LEN_HDR + BLOCK_SIZE
#define RCV_BUF_LEN         425984
#define SND_BUF_LEN         212992
#define INITIAL_WINDOW_SIZE 32
#define MAX_WINDOW_SIZE     1024

#define CLIENT_PORT                9001
#define CLIENT_SLEEP_US_PACKET     10
#define CLIENT_SLEEP_US_WINDOW     10000
#define CLIENT_NUM_ACK_PERCENTAGES 10

#define SERVER_QUEUE_SIZE 5001 // max memory consumption ~ 230 MiB

#define PING_COUNT 10

#define MSG_REQUEST_UPLOAD    1
#define MSG_ACK_UPLOAD        3
#define MSG_REJECT_UPLOAD     5
#define MSG_END_UPLOAD        7
#define MSG_ACK_END_UPLOAD    11
#define MSG_DATA              13
#define MSG_SET               17
#define MSG_PING              19

/* types */
typedef struct sockaddr sa_t;
typedef struct sockaddr_in sai_t;
typedef struct in_addr ia_t;

typedef struct upload {
	uint16_t number;
	uint32_t len_low;
	uint32_t len_high;
	char     fname[LEN_FNAME];
} upload_t;

typedef struct header {
	uint8_t  msg_type;
	uint16_t upload_number;
	uint32_t chunk_number;
	uint32_t packet_number;
} hdr_t;

typedef struct raw {
	int       len;
	ia_t      ip;
	uint16_t  port;
	char     *buf;
} raw_t;

typedef struct send_ctx {
	int             socket;
	sai_t          *addr;
	upload_t       *upload;
	set_t          *progress;        // shared mutable
	int             progress_update; // shared mutable
	uint32_t        window_size;
	uint32_t        chunk_number;    // shared mutable
	uint32_t        packet_number;
	pthread_mutex_t mutex;
} send_ctx_t;

typedef struct rcv_ctx {
	upload_t *upload;
	set_t    *packet_set;
	int      fd;
	uint32_t chunk_number;
} rcv_ctx_t;


/* common */
void die(char *s);

char* serialize_hdr(hdr_t *hdr); // TODO should take buf as argument, not allocate

hdr_t* deserialize_hdr(char *buf);

void print_hdr(hdr_t *hdr);

upload_t* make_upload(char *fname);
void print_upload(upload_t *upload);
char* serialize_upload(upload_t *upload); // TODO should take buf as argument, not allocate
uint64_t len_upload(upload_t *upload);
upload_t* deserialize_upload(char *buf);

sai_t from_ip_port(ia_t ip, uint16_t port);

int send_msg(int s, hdr_t *hdr, char *buf, ssize_t len, sa_t *addr);

int bind_socket(sai_t bind_addr, struct timeval *timeout);

sai_t parse_addr(char *aip, char *aport);

long time_diff(struct timespec *start, struct timespec *stop);

off_t packet_offset(uint32_t chunk_number, uint32_t packet_number);

uint32_t chunk_count(uint64_t len);
uint32_t packet_count(uint32_t chunk_number, uint64_t len);
uint32_t packet_len(uint32_t chunk_number, uint32_t packet_number, uint64_t len);

int get_rcv_buf_len(int socket);
void set_rcv_buf_len(int socket);

int get_snd_buf_len(int socket);
void set_snd_buf_len(int socket);

/*
 * using socket `sck`
 * repeatedly send message to address `send_addr`
 * consisting of header `hdr` + optional payload `buf` of length `len`
 *
 * after every send, listen for incoming messages on socket `sck`
 * pass every received message to callback `cb` along with context `ctx`
 * if callback returns non-NULL value, return with said value
 *
 * if `max_timeouts` is positive, abort after `max_timeouts` timeouts, returning NULL
 */
void* send_receive(int sck, sai_t *send_addr, hdr_t *hdr, char *buf, size_t len, int max_timeouts, void *ctx, void* (*cb)(raw_t *raw, void *ctx));

/* ping */
void do_ping(char *aip, char *aport);
void* ping_callback(raw_t *raw, void *ctx);

/* client */
void do_send(char *aip, char *aport, char *fname);

void request_upload(send_ctx_t *ctx);
void* request_upload_callback(raw_t *raw, void *ctx);

void end_upload(send_ctx_t *ctx);
void* end_upload_callback(raw_t *raw, void *ctx);

void send_file(send_ctx_t *ctx);
void send_chunk(send_ctx_t *ctx, int fd);

int send_window(send_ctx_t *ctx, int fd, uint32_t pcount);

void* run_client_listen(void *arg);
void client_handle_packet(send_ctx_t *ctx, raw_t *raw);

/* server */
void do_listen();

void* run_server_listen(void *arg);

void handle_packet(raw_t *raw);

void handle_request_upload(char *buf, int len, ia_t ip, uint16_t port);
void handle_end_upload(hdr_t *hdr, ia_t ip, uint16_t port);
void ack_upload(upload_t *upload, ia_t ip, uint16_t port);
void reject_upload(upload_t *upload, ia_t ip, uint16_t port);
void handle_data(hdr_t *hdr, char *buf, int len, ia_t ip, uint16_t port);
void handle_set(hdr_t *hdr, ia_t ip, uint16_t port);
void handle_ping(hdr_t *hdr, ia_t ip, uint16_t port);

#endif
